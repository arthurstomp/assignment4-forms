/*global require */
/**
 * Created by theotheu on 24-12-13.
 */
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    schema = mongoose.Schema;

var userSchema = schema({
  _id : {type: schema.Types.ObjectId, ref:"users"},
});

/* Schema definitions */
// Schema types @see http://mongoosejs.com/docs/schematypes.html
var schemaName = schema({
    name: {type: String, required: true},
    description: {type: String},
    modificationDate: {type: Date, "default": Date.now},
    users: [schema.Types.ObjectId],
}, {collection: "groups"});


var modelName = "Group";
module.exports = mongoose.model(modelName, schemaName);
