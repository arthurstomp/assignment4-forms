
#Install and configure

## Installing and setting MongoDB

Quick guide For all the installation guides, visite [mongo website](http://docs.mongodb.org/manual/installation/).

Once installed the MongoDB you have access to two functions, at least on MacOX, `mongo` and `mongod`. `mongo` open a console to your mongodb database. `mongod` start an instance of MongoDB and it stores the data at `/data/db` ( if you dot have this directory, create it, otherwise 'mongod' will exit with an error).

### TODO - Make `mongod --fork` work

## Install Robomongo

[Robomongo](http://robomongo.org/) is a MongoDB management tool. A graphic tool for Mongo. Yeah!

![alt text](http://robomongo.org/images/full-power-of-shell.png "robomongo")

##Import data
1. Go to the `data` directory
1. Execute `mongorestore -d cria-dev seed`


## Run the server
1. Go to the `server` directory
1. Install node modules bij executing `npm install`
1. Run the server by executing `nodemon` or `npm start`

#Your assignment

## Create a model for Ikea products


#Prepare a deployment

## Export data
1. Change your directory to data
1. Execute `mongodump -d s12345678 -o seed`

# Relations in MongoDB

Sharding : Documents can be in different servers and might not be available on time for the request.

### Nested Documents

```javascript
//User schema

{
  email : "john@example.com",
  password : "12345678",
  phonenumbers : [{
    cod : "55",
    local : "79",
    number : "32435247"
  }]
}
```

### Reference ObjectId

```javascript
//User schema

var phoneNumberSchema = schema({
  _id : {type: schema.Types.ObjectId, ref:"phone_numbers"},
});

{
  email: "john@example.com",
  password: "12345678",
  phonenumbers: [phoneNumberSchema],
}
```

OBS : the `ref:<collection name>` enable the use of `populate` to expand relations.

##### Authorization Structure

A authorization structure most follow the patter : users <--> groups <--> privileges;

# Query based on array sub-documents

```javascript
{
 _id: 4,
 zipcode: "63109",
 students: [
              { name: "barney", school: 102, age: 7 },
              { name: "ruth", school: 102, age: 16 },
           ]
}
```

```javascript
db.schools.find( { zipcode: "63109" },
                 { students: { $elemMatch: { school: 102 } } } )

```
=======
